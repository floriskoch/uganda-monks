// Server API makes it possible to hook into various parts of Gridsome
// on server-side and add custom data to the GraphQL data layer.
// Learn more: https://gridsome.org/docs/server-api/

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`
module.exports = function(api) {
  api.createPages(async ({ graphql, createPage }) => {
    createPage({
      path: '/',
      component: './src/templates/HomeView.vue'
    });

    const monkPages = [
      { path: '/monks/father-john-bosco', template: 'MonkJohnBosco' },
      { path: '/monks/brother-elijah', template: 'MonkElijah' },
      { path: '/monks/brother-richard', template: 'MonkRichard' },
      { path: '/monks/brother-delphin', template: 'MonkDelphin' },
      { path: '/monks/brother-michael', template: 'MonkMichael' }
    ];

    monkPages.forEach(({ path, template }, i) => {
      const prev = i === 0 ? '/' : monkPages[i - 1].path;
      const next = monkPages[i + 1] ? monkPages[i + 1].path : '/';
      createPage({
        path,
        component: `./src/templates/${template}.vue`,
        context: {
          path,
          prev,
          next
        }
      });
    });
  });
};
