// This is the main.js file. Import global CSS and scripts here.
// The Client API can be used here. Learn more: gridsome.org/docs/client-api
import '@/assets/styles/main.css';

import DefaultLayout from '@/layouts/Default.vue';

export default function(Vue, { router, head, isClient }) {
  // Set default layout as a global component
  Vue.component('Layout', DefaultLayout);

  head.link.push({
    rel: 'stylesheet',
    href:
      'https://fonts.googleapis.com/css?family=Montserrat:300,500,700&display=swap'
  });

  head.meta.push({
    key: 'og:description',
    name: 'og:description',
    property: 'og:description',
    content: `The Monks of Kijonjo - Searching for Silence`,
  })

  head.meta.push({
    key: 'og:title',
    name: 'og:title',
    property: 'og:title',
    content: `The Monks of Kijonjo - Searching for Silence`,
  })

  head.meta.push({
    key: 'twitter:description',
    name: 'twitter:description',
    property: 'twitter:description',
    content: `The Monks of Kijonjo - Searching for Silence`,
  })

  head.meta.push({
    key: 'twitter:title',
    name: 'twitter:title',
    property: 'twitter:title',
    content: `The Monks of Kijonjo - Searching for Silence`,
  })

  head.meta.push({
    key: 'og:image',
    name: 'og:image',
    property: 'og:image',
    content: 'http://monksofkijonjo.com/social.jpg',
  })
}
