// This is where project configuration and plugin options are located.
// Learn more: https://gridsome.org/docs/config

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

const tailwind = require('tailwindcss');
const purgecss = require('@fullhuman/postcss-purgecss')({
  // Specify the paths to all of the template files in your project
  content: [
    './src/**/*.vue',
    './src/**/*.js',
    './src/**/*.jsx',
    './src/**/*.html',
    './src/**/*.pug',
    './src/**/*.md'
  ],

  // Include any special characters you're using in this regular expression
  defaultExtractor: content => content.match(/[A-Za-z0-9-_:/]+/g) || []
});

const postcssPlugins = [tailwind()];

if (process.env.NODE_ENV === 'production') {
  // postcssPlugins.push(purgecss());
}

module.exports = {
  // pathPrefix: '/uganda-monks/',
  outputDir: 'public',
  siteName: 'The Monks of Kijonjo',
  plugins: [
    {
      use: '@gridsome/source-filesystem',
      options: {
        typeName: 'PageView',
        path: './content/pages/**/*.yaml'
      }
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        typeName: 'MonkView',
        baseDir: './content',
        path: 'monks/**/*.yaml'
      }
    },
    {
      use: 'gridsome-plugin-gtag',
      options: {
        config: { id: 'UA-170310611-1' }
      }
    }
  ],
  templates: {
    PageView: '/pages/:title'
  },
  css: {
    loaderOptions: {
      postcss: {
        plugins: postcssPlugins
      }
    }
  },
  chainWebpack: config => {
    const svgRule = config.module.rule('svg');
    svgRule.uses.clear();
    svgRule.use('vue-svg-loader').loader('vue-svg-loader');
  }
};
