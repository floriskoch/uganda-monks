module.exports = {
  theme: {
    fontFamily: {
      body: ['Montserrat', 'sans-serif']
    },
    fontSize: {
      xs: '0.75rem',
      sm: '0.875rem',
      base: '1rem',
      lg: '1.125rem',
      xl: '1.25rem',
      '2xl': '1.625rem',
      '3xl': '1.875rem',
      '4xl': '2.1875rem',
      '5xl': '2.625rem',
      '6xl': '3.125rem'
    },
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      black: '#000',
      white: '#fff',
      green: '#7E7F61',
      yellow: '#E2AF3E',
      orange: '#D07C29',
      camel: '#D6B785',
      beige: '#E8CDB3',
      brown: '#3B170B'
    },
    extend: {
      inset: {
        '1/2': '50%'
      },
      spacing: {
        '2/5': '40%'
      },
      maxWidth: {
        xxs: '18rem'
      },
      maxHeight: {
        80: '80vh'
      },
      width: {
        '1/7': '14.2857143%',
        '2/7': '28.5714286%',
        '3/7': '42.8571429%',
        '4/7': '57.1428571%',
        '5/7': '71.4285714%',
        '6/7': '85.7142857%'
      },
      zIndex: {
        60: '60'
      }
    }
  },
  variants: {},
  plugins: []
};
